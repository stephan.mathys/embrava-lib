package ch.hotmilk.embrava.core;

import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

@TestInstance(Lifecycle.PER_CLASS)
@ExtendWith(MockitoExtension.class)
public class BaseTest {

    @org.junit.jupiter.api.BeforeAll
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }
}
