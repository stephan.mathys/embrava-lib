package ch.hotmilk.embrava.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Map;

import org.hid4java.HidServices;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import ch.hotmilk.embrava.core.devices.BlynclightDevice;
import ch.hotmilk.embrava.core.listeners.EmbravaListener;
import ch.hotmilk.embrava.core.services.EmbravaService;

class EmbravaServiceTest extends BaseTest {

	@Mock
	private Map<String, BlynclightDevice> devices;

	@Mock
	private HidServices hidService;

	@Mock
	private EmbravaListener listener;

	@InjectMocks
	private EmbravaService service;

	@Test
	public void test() {
		when(service.test()).thenReturn(1);

		assertEquals(service.test(), 1);
	}

	@Test
	public void testAddListener() {
		service.addBlynclightListener(service);
		verify(listener, times(1)).addListener(service);
	}

	@Test
	public void testRemoveListener() {
		service.removeBlynclightListener(service);
		verify(listener, times(1)).removeListener(service);
	}

	@Test
	public void testTestingLight() {
		service.testingLight();
		verify(devices, times(3)).get("1").prepareDataForColor(any(BlynclightColors.class));
		verify(devices, times(1)).get("1").processData();
		verify(devices, times(1)).get("1").turnOff();

	}

}
