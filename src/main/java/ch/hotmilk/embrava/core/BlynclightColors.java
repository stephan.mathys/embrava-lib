package ch.hotmilk.embrava.core;

public enum BlynclightColors {
	GREEN(0, 0, 127, "green"), RED(127, 0, 0, "red"), BLUE(0, 127, 0, "blue");

	private byte red;
	private byte blue;
	private byte green;
	private String color;

	BlynclightColors(int red, int blue, int green, String color) {
		this.green = (byte) green;
		this.red = (byte) red;
		this.blue = (byte) blue;
		this.color = color;
	}

	public byte green() {
		return green;
	}

	public byte red() {
		return red;
	}

	public byte blue() {
		return blue;
	}

	public String color() {
		return color;
	}

	public static BlynclightColors findColor(String color) {
		for (BlynclightColors c : values()) {
			if (c.color().equalsIgnoreCase(color)) {
				return c;
			}
		}
		return null;
	}
}
