package ch.hotmilk.embrava.core.devices;

import java.nio.ByteBuffer;

import org.hid4java.HidDevice;

import ch.hotmilk.embrava.core.BlynclightColors;
import ch.hotmilk.embrava.core.BlynclightLightControl;
import ch.hotmilk.embrava.core.BlynclightProduct;
import ch.hotmilk.embrava.core.BlynclightRing;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class BlynclightDevice {
	private static final byte FEATURE_REPORT = 0x00;
	private HidDevice device;
	private BlynclightProduct blynclightProduct;
	private ByteBuffer data;

	private class Sending implements Runnable {
		private ByteBuffer data;

		Sending(ByteBuffer data) {
			this.data = data;
		}

		@Override
		public void run() {
			openDevice();
			log.debug("Data will be sent to device");
			data.rewind();
			if (log.isTraceEnabled()) {
				log.trace("The following data will be sent to device: ");
				while (data.hasRemaining()) {
					log.trace("data[{}]: {}", data.position(), data.get());
				}
				data.rewind();
			}
			int deviceResponse = device.write(data.array(), data.capacity(), FEATURE_REPORT);
			log.debug("The response from the device is " + deviceResponse);

		}

		private void openDevice() {
			if (!device.isOpen()) {
				log.info("Device will be opend");
				if (!device.open()) {
					throw new RuntimeException("Device could not open. Probably the device isn't here");
				}
			}
			// TODO Maybe we have an other device to open
		}
	}

	public BlynclightDevice(HidDevice device) {
		this.device = device;
		data = ByteBuffer.allocate(8);
		initializeData();
		blynclightProduct = BlynclightProduct.getProduct(device.getVendorId(), device.getProductId());
	}

	public Integer processData() {
		Sending sending = new Sending(data);
		Thread t1 = new Thread(sending);
		t1.start();
		return 1;
	}

	/**
	 * Method to turn the light off
	 * 
	 * @return hwo many data would be processed
	 */
	public Integer turnOff() {
		prepareDataForLightControl(BlynclightLightControl.LIGHT_OFF);
		return processData();
	}

	public void prepareDataForColor(BlynclightColors color) {
		this.data.put(0, color.red());
		this.data.put(1, color.blue());
		this.data.put(2, color.green());
	}

	public void prepareDataForLightControl(BlynclightLightControl lightControl) {
		this.data.put(3, lightControl.getLightControl());
	}

	public void prepareDataForRingTone(BlynclightRing ringTone) {
		this.data.put(4, ringTone.getRingTone());
	}

	private void initializeData() {
		prepareDataForColor(BlynclightColors.RED);
		prepareDataForLightControl(BlynclightLightControl.LIGHT_ON);
		prepareDataForRingTone(BlynclightRing.SOUND_OFF);
		this.data.put(5, (byte) 0x00);
		this.data.put(6, (byte) 0x00);
		this.data.put(7, (byte) 0x00);
	}

	public Integer test() {
		for (int i = 0; i < 20; i++) {
			log.debug("Add " + i + " to device");
			this.data.put(3, (byte) i);
			processData();
		}

		return 1;
	}

	public Integer getProductId() {
		return device.getProductId();
	}

	public String getId() {
		return device.getId();
	}

	public int getInterfaceNumber() {
		return device.getInterfaceNumber();
	}

	public String getManufacterer() {
		return device.getManufacturer();
	}

	public String getPath() {
		return device.getPath();
	}

	public String getProduct() {
		return device.getProduct();
	}

	public Integer getVendorId() {
		return device.getVendorId();
	}

	public String getSerialNumber() {
		return device.getSerialNumber();
	}

	public BlynclightProduct getBlynclightProduct() {
		return blynclightProduct;
	}

	public void setBlynclightProduct(BlynclightProduct blynclightProduct) {
		this.blynclightProduct = blynclightProduct;
	}
}