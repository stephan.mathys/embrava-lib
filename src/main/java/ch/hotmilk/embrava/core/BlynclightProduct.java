package ch.hotmilk.embrava.core;

public enum BlynclightProduct {
	STANDARD("Blynclight", 3667, 9495), MINI("Blynclight Mini", 9999, 9999);

	private String product;
	private Integer productId;
	private Integer vendorId;

	private BlynclightProduct(String product, Integer vendorId, Integer productId) {
		this.product = product;
		this.productId = productId;
		this.vendorId = vendorId;
	}

	public static Boolean isProduct(Integer vendorId, Integer productId) {
		return getProduct(vendorId, productId) != null;

	}

	public static BlynclightProduct getProduct(Integer vendorId, Integer productId) {
		for (BlynclightProduct p : values()) {
			if (p.getVendorId().equals(vendorId) && p.getProductId().equals(productId)) {
				return p;
			}
		}
		return null;
	}

	public String getProduct() {
		return product;
	}

	public Integer getProductId() {
		return productId;
	}

	public Integer getVendorId() {
		return vendorId;
	}
}
