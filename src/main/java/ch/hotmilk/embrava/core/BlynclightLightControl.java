package ch.hotmilk.embrava.core;

/**
 * 0 = ON; 1 = OFF; 4 = FAST; 12= SLOW
 * 
 * @author Stephan Mathys
 *
 */
public enum BlynclightLightControl {

	LIGHT_ON(0), LIGHT_OFF(1), SLOW(12), FAST(4);

	private byte lightControl;

	private BlynclightLightControl(int lightControl) {
		this.lightControl = (byte) lightControl;
	}

	public byte getLightControl() {
		return lightControl;
	}

}
