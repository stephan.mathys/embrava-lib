package ch.hotmilk.embrava.core.listeners;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hid4java.HidDevice;
import org.hid4java.HidServicesListener;
import org.hid4java.event.HidServicesEvent;

import ch.hotmilk.embrava.core.BlynclightEvent;
import ch.hotmilk.embrava.core.IBlynclightListener;
import ch.hotmilk.embrava.core.services.EmbravaService;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class EmbravaListener implements HidServicesListener {
	private List<HidDevice> devices = new ArrayList<HidDevice>();
	private List<IBlynclightListener> listeners = new ArrayList<IBlynclightListener>();
	private final String ATTACHED = "attached";
	private final String DETACHED = "detached";

	@Override
	public void hidDeviceAttached(HidServicesEvent event) {
		log.debug("device attached with productID " + event.getHidDevice().getProductId());
		notifyListeners(event, ATTACHED);
	}

	@Override
	public void hidDeviceDetached(HidServicesEvent event) {
		log.debug("device detached with productID " + event.getHidDevice().getProductId());
		notifyListeners(event, DETACHED);
	}

	private void notifyListeners(HidServicesEvent event, String whatToNotify) {
		log.debug("notify the listeners for Blynclight events");
		HidDevice hidDevice = event.getHidDevice();
		if (isBlynclightDevice(hidDevice)) {
			log.debug("Add the device to our devices");
			devices.add(hidDevice);
		}
		BlynclightEvent listenerEvent = new BlynclightEvent();
		listenerEvent.setProductID(hidDevice.getProductId());
		listenerEvent.setProduct(hidDevice.getProduct());

		Iterator<IBlynclightListener> it = listeners.iterator();
		while (it.hasNext()) {
			IBlynclightListener blynclightListener = it.next();
			log.debug("We notify the listener " + blynclightListener.toString());
			switch (whatToNotify) {
			case ATTACHED:
				blynclightListener.attached(listenerEvent);
				break;
			case DETACHED:
				blynclightListener.removed(listenerEvent);
				break;
			}
		}
	}

	@Override
	public void hidFailure(HidServicesEvent event) {
		log.debug("we had a failure");
	}

	public Boolean isBlynclightDevice(HidDevice device) {
		return device.getProduct().startsWith(EmbravaService.PRODUCT);
	}

	public void addListener(IBlynclightListener listener) {
		log.debug("Listener added");
		listeners.add(listener);
	}

	public void removeListener(IBlynclightListener listener) {
		log.debug("Listener removed");
		listeners.remove(listener);
	}

	public Boolean isEmpty() {
		return listeners.isEmpty();
	}

	@Override
	public void hidDataReceived(HidServicesEvent arg0) {
		// TODO Auto-generated method stub

	}
}
