package ch.hotmilk.embrava.core;

public enum BlynclightRing {

    RING_1(17), RING_2(18), RING_3(20), RING_4(21), RING_5(26), RING_6(25), BLIM_1(15), BLIM_2(16), BLIM_3(19),
    BLIM_4(20), BLIM_5(22), FUTURE_1(23), FUTURE_2(24), SOUND_OFF(0);
    private byte ringTone;
    
    private BlynclightRing(int ringTone) {
        this.ringTone = (byte) ringTone;
    }

    public byte getRingTone() {
        return ringTone;
    }
}
//data[4]