package ch.hotmilk.embrava.core.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.management.RuntimeErrorException;

import org.apache.commons.lang3.NotImplementedException;
import org.hid4java.HidDevice;
import org.hid4java.HidManager;
import org.hid4java.HidServices;

import ch.hotmilk.embrava.core.BlynclightColors;
import ch.hotmilk.embrava.core.BlynclightEvent;
import ch.hotmilk.embrava.core.BlynclightLightControl;
import ch.hotmilk.embrava.core.BlynclightProduct;
import ch.hotmilk.embrava.core.BlynclightRing;
import ch.hotmilk.embrava.core.IBlynclightListener;
import ch.hotmilk.embrava.core.IEmbrava;
import ch.hotmilk.embrava.core.devices.BlynclightDevice;
import ch.hotmilk.embrava.core.listeners.EmbravaListener;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class EmbravaService implements IEmbrava, IBlynclightListener {
	public static String PRODUCT = "Blynclight";
	private HidServices hidService = null;
	private EmbravaListener listener = null;
	private Map<String, BlynclightDevice> devices;
	private BlynclightDevice currentDevice;

	public EmbravaService() {
		this.hidService = getHidServiceInstance();
		this.devices = getAllBlynclightDevices();
		this.listener = new EmbravaListener();
		this.listener.addListener(this);
		this.hidService.addHidServicesListener(listener);
		if (!setFirstBlynclight()) {
			throw new RuntimeErrorException(null, "No Blynclight found on this computer");
		}
	}

	// gets the HidService
	private HidServices getHidServiceInstance() {
		if (hidService == null) {
			log.debug("Initialize the HidService");
			hidService = HidManager.getHidServices();
			// Because we get an jre crash
			hidService.stop();
		}
		return hidService;
	}

	private Map<String, BlynclightDevice> getAllBlynclightDevices() {
		Map<String, BlynclightDevice> d = new HashMap<String, BlynclightDevice>();
		List<HidDevice> attachedHidDevices = hidService.getAttachedHidDevices();
		for (HidDevice hidDevice : attachedHidDevices) {
			if (isDeviceBlynclight(hidDevice)) {
				d.put(hidDevice.getId(), new BlynclightDevice(hidDevice));
			}
			getHidDeviceInfo(hidDevice);
		}
		return d;
	}

	private void getHidDeviceInfo(HidDevice hidDevice) {
		log.debug("==============================================");
		log.debug("Device Id: " + hidDevice.getId());
		log.debug("Device interface number: " + hidDevice.getInterfaceNumber());
		log.debug("Device manufacturer: " + hidDevice.getManufacturer());
		log.debug("Device path: " + hidDevice.getPath());
		log.debug("Device product: " + hidDevice.getProduct());
		log.debug("Device product id: " + hidDevice.getProductId());
		log.debug("Device release number: " + hidDevice.getReleaseNumber());
		log.debug("Device vendor id: " + hidDevice.getVendorId());
		log.debug("Device serial number: " + hidDevice.getSerialNumber());
	}

	public Boolean isDeviceBlynclight(HidDevice device) {
		return BlynclightProduct.isProduct(device.getVendorId(), device.getProductId());
	}

	@Override
	public Integer switchLight(Boolean turnOn) {
		log.debug("not implemented");
		throw new NotImplementedException("This method is not implemented yet");
	}

	@Override
	public Integer isLightSwitchedOn() {
		log.debug("not implemented");
		throw new NotImplementedException("This method is not implemented yet");
	}

	@Override
	public void addBlynclightListener(IBlynclightListener listener) {
		this.listener.addListener(listener);
	}

	@Override
	public void removeBlynclightListener(IBlynclightListener listener) {
		this.listener.removeListener(listener);
	}

	@Override
	public Integer testingLight() {
		try {
			log.debug("Send RED to device");
			currentDevice.prepareDataForColor(BlynclightColors.RED);
			currentDevice.processData();
			java.util.concurrent.TimeUnit.SECONDS.sleep(3);

			log.debug("Send BLUE to device");
			currentDevice.prepareDataForColor(BlynclightColors.BLUE);
			currentDevice.processData();
			java.util.concurrent.TimeUnit.SECONDS.sleep(3);

			log.debug("Send GREEN to device");
			currentDevice.prepareDataForColor(BlynclightColors.GREEN);
			currentDevice.processData();
			java.util.concurrent.TimeUnit.SECONDS.sleep(3);

			currentDevice.turnOff();
		} catch (InterruptedException e) {
			log.debug("current timeUnit gave an exception");
			e.printStackTrace();
		}
		return 1;
	}

	@Override
	public Integer testBlinkControl() {
		try {
			currentDevice.prepareDataForLightControl(BlynclightLightControl.FAST);
			currentDevice.processData();
			java.util.concurrent.TimeUnit.SECONDS.sleep(3);

			currentDevice.prepareDataForLightControl(BlynclightLightControl.SLOW);
			currentDevice.processData();
			java.util.concurrent.TimeUnit.SECONDS.sleep(3);

			currentDevice.turnOff();
		} catch (InterruptedException e) {
			log.debug("current timeUnit gave an exception");
			e.printStackTrace();
		}
		return 1;
	}

	@Override
	public Integer testBrightControl() {
		currentDevice.test();
		return 1;
	}

	@Override
	public Integer findDeviceProductID() {
		return currentDevice.getProductId();
	}

	@Override
	public Integer test() {
		return currentDevice.test();
	}

	@Override
	public void removed(BlynclightEvent event) {
		log.debug("removed");
		throw new NotImplementedException("This method is not implemented yet");
	}

	@Override
	public void attached(BlynclightEvent event) {
		log.debug("attached");
		throw new NotImplementedException("This method is not implemented yet");
	}

	@Override
	public Integer startScanning() {
		throw new NotImplementedException("Scanning is not workting yet");
//		hidService.start();
	}

	@Override
	public Integer stopScanning() {
		throw new NotImplementedException("Scanning is not workting yet");
//        hidService.stop();
	}

	@Override
	public List<String> getAllDeviceIds() {
		List<String> keys = new ArrayList<String>();
		keys.addAll(devices.keySet());
		return keys;
	}

	@Override
	public BlynclightProduct getProduct() {
		return currentDevice.getBlynclightProduct();
	}

	@Override
	public boolean isDeviceStillAvailable() {
		return currentDevice != null;
	}

	@Override
	public void sendToDevice(BlynclightColors color, BlynclightLightControl lightControl, BlynclightRing ringTone,
			Integer howManySeconds) {
		currentDevice.prepareDataForColor(color);
		currentDevice.prepareDataForLightControl(lightControl);
		currentDevice.prepareDataForRingTone(ringTone);
		currentDevice.processData();
		try {
			java.util.concurrent.TimeUnit.SECONDS.sleep(howManySeconds);
		} catch (InterruptedException e) {
			log.error("current Time gave an error", e);
		} finally {
			currentDevice.turnOff();
		}
	}

	public void setCurrentDevice(String deviceId) {
		currentDevice = devices.get(deviceId);
	}

	@Override
	public Boolean setFirstBlynclight() {
		List<String> allDeviceIds = getAllDeviceIds();
		if (!allDeviceIds.isEmpty()) {
			currentDevice = devices.get(allDeviceIds.get(0));
			return true;

		}
		return false;
	}
}
