package ch.hotmilk.embrava.core;

public interface IBlynclightListener {

    public void removed(BlynclightEvent event);

    public void attached(BlynclightEvent event);

}
