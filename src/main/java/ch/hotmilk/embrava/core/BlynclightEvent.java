package ch.hotmilk.embrava.core;

import lombok.Data;

@Data
public class BlynclightEvent {

	private Integer productID;
	private String product;
}
