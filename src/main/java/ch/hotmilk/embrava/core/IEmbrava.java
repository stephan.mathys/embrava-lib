package ch.hotmilk.embrava.core;

import java.util.List;

public interface IEmbrava {

	/**
	 * Switch the light ON and OFF. The standard color "green" is used for this
	 * 
	 * @param turnOn Boolean if the light should switch OFF or ON
	 * @return If the light could switched 1 == Light switched to NO 0 == Light
	 *         switched to OFF -1 == something went wrong
	 * 
	 */
	public Integer switchLight(Boolean turnOn);

	/**
	 * @return which state has the light 1 == Light is ON 0 == Light is OFF -1 ==
	 *         Something went wrong
	 */
	public Integer isLightSwitchedOn();

	public void addBlynclightListener(IBlynclightListener listener);

	public void removeBlynclightListener(IBlynclightListener listener);

	Boolean setFirstBlynclight();

	/**
	 * 
	 * @return Everything greater than 1 is ok; -1 == something went wrong
	 */

	public Integer testingLight();

	public Integer testBlinkControl();

	public Integer testBrightControl();

	public Integer findDeviceProductID();

	public Integer test();

	public Integer startScanning();

	public Integer stopScanning();

	/**
	 * 
	 * @return A list of all IDs from currently plugged-in devices
	 */
	public List<String> getAllDeviceIds();

	public BlynclightProduct getProduct();

	public boolean isDeviceStillAvailable();

	public void sendToDevice(BlynclightColors color, BlynclightLightControl lightControl, BlynclightRing ringTone,
			Integer howManySeconds);
}
