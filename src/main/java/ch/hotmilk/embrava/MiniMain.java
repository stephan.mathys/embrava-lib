package ch.hotmilk.embrava;

import ch.hotmilk.embrava.core.IEmbrava;
import ch.hotmilk.embrava.core.services.EmbravaService;
import lombok.extern.log4j.Log4j2;

/**
 * -Dlog4j.configuration=file:"FILE_PATH"
 * 
 * @author sma
 *
 */
@Log4j2
public class MiniMain {

	public static void main(String[] args) {
		log.debug("Starting MiniMain test");
		IEmbrava service = new EmbravaService();
		service.setFirstBlynclight();
		service.testingLight();
		service.testBlinkControl();
		service.test();
		log.debug("Ending MiniMain test");

	}

}
